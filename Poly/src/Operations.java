
public abstract class Operations  {

	

	static void addDistinctTerms(Polynom P1, Polynom P2,Polynom rez)
	{
		int ok=1;
		for(Monom m1:P1.Polynomial){
			ok =1;
			for(Monom m2:P2.Polynomial){
				if(m1.getRank()==m2.getRank())  ok=0;
			}   
			if(ok==1) {Monom b = new Monom(); b.setCoefficient(m1.getCoefficient());      
                           b.setRank(m1.getRank()); rez.addMonom(m1);
            }
      }
		
	}
	
	  static Polynom inverse ( Polynom P)
	{
		
		  Polynom rez=new Polynom();
          for(Monom m:P.Polynomial){
        	  m.setCoefficient(0-m.getCoefficient());
        	  rez.addMonom(m);
        	  
          }		 
          fix0Coeff(rez);
		  return rez ;
	}
	 
		static Polynom add(Polynom P1, Polynom P2){
			Polynom rez = new Polynom();
			for(Monom m1:P1.Polynomial){
				for(Monom m2:P2.Polynomial){
					if(m1.getRank()==m2.getRank()) { 
					   Monom a=new Monom();
						a.setCoefficient(m1.getCoefficient()+m2.getCoefficient());
						a.setRank(m1.getRank());    rez.addMonom(a);
					}
				}
			}	
		    addDistinctTerms(P1, P2, rez);
			addDistinctTerms(P2, P1, rez);
			fix0Coeff(rez);
			return rez;
	}
	  
	  
	 static Polynom multiply(Polynom P1, Polynom P2)
	  {
		   
		  Polynom rez = new Polynom();
		  for(Monom a:P1.Polynomial){
			  for(Monom b:P2.Polynomial){
				  Monom newMon=new Monom();
				  int coeff=(int) ((a.getCoefficient())*b.getCoefficient());
				  int rank =a.getRank()+b.getRank();
				  newMon.setCoefficient(coeff);
				  newMon.setRank(rank);
	
				  rez.addMonom(newMon);
				  fix0Coeff(rez);
			  }
			  
		  }
		  fix0Coeff(rez);
		  return rez; 
	  }
	 

	
	  static Polynom[] divide(Polynom P1, Polynom P2)
	  {
		    Polynom [] rez = new Polynom[2];
		    Polynom quotient = new Polynom();
		    Polynom rest = new Polynom();
		    /*
		     * quotient<-0
		     * rest<-P1
		     *  while deg(rest)>= deg(P2) and rest !=0
		     *  begin
		     *  temp <- MaxMon(rest)/MaxMon(P2)
		     * quotient <- quotient+temp
		     * rest<-rest-temp*P2
		     * end while	     * 
		     */
		    // getMaxMon - returns monom with max pwoer 
		    // MultiplyMon - multiplies a Monom with a Polynomial. returns Polynom
		    rest=P1;
		    while(rest.isEmpty()==false && rest.getMaxMon().getRank()>=P2.getMaxMon().getRank()){
		    	Polynom aux = new Polynom();
		    	Polynom aux2=rest;
		    	Polynom aux3= new Polynom();
		    	Monom temp= new Monom();
		    	
		    	temp.setRank(rest.getMaxMon().getRank()-P2.getMaxMon().getRank());
                temp.setCoefficient(rest.getMaxMon().getCoefficient()/P2.getMaxMon().getCoefficient());	
                
                quotient.addMonom(temp);    // start construct quotient
                aux3.addMonom(temp); //temp as polynomial in order to perform multiplication
                aux=multiply(P2, aux3);    //aux =temp*p2
                rest=subtract(aux2, aux);  //rest=rest-aux
               
               
		    }
		    rez[0]=quotient;
		    fix0Coeff(rez[0]);
		    rez[1]=rest;
		    fix0Coeff(rez[1]);
		   return rez;
				
		  
		 
	  }
	 static Polynom subtract(Polynom P1, Polynom P2){
		 Polynom rez = new Polynom();
		 P2=Operations.inverse(P2);
		 rez=add(P1,P2);
		 
		 fix0Coeff(rez);
		 return rez;
		 
	 } 
	  
	  
	  static Polynom derive(Polynom P1){
		  
		  Polynom rez = new Polynom();
		  for(Monom aux:P1.Polynomial){
			  if(aux.getRank()!=0){
				rez.addMonom(aux);
			  }
			  else continue; 
		  }
		  
		  
		  for(Monom newMon:rez.Polynomial){
			  newMon.setCoefficient(newMon.getRank()*newMon.getCoefficient());
			  newMon.setRank(newMon.getRank()-1);
		  }
		  fix0Coeff(rez);
		  return rez;
	  }
	  
	  
	  static Polynom integrate ( Polynom P1)
	  {
		  Polynom rez = new Polynom();
		  
		  for(Monom a: P1.Polynomial){
			  Monom newMon = new Monom();
			  newMon.setRank(a.getRank()+1);
			  newMon.setCoefficient(a.getCoefficient()/(a.getRank()+1));
			  rez.Polynomial.add(newMon);
		  }
	      fix0Coeff(rez);
		  return rez;
		  
		  
	  }
	  static void fix0Coeff(Polynom P){
			
		  Polynom toRemove = new Polynom();
		  for(Monom a : P.Polynomial)
			{
				if(a.getCoefficient()==0)
					toRemove.Polynomial.add(a);
				
			}
		  P.Polynomial.removeAll(toRemove.Polynomial);
		}
	  
	  static boolean polyEquals(Polynom P1, Polynom P2)
	  {
		  Polynom rez = subtract(P1,P2);
		  fix0Coeff(rez);
		  if(rez.Polynomial.isEmpty()==true)
			  return true;
		  else
			  return false;
		  
	  }
	  
}
