
public class Monom implements Comparable  {

	private double coefficient;
	private int rank;
	
	public  Monom()
	{}
	
	public void setCoefficient(double coefficient)
	{
		this.coefficient=coefficient;
	}
	
	public double getCoefficient()
	{
		return this.coefficient;
	}
	
	
	public void setRank(int rank)
	{
		this.rank=rank;
	}
	
	public int getRank()
	{
		return this.rank;
	}



	@Override
	public int compareTo(Object o) {
		int cmprnk = ((Monom)o).getRank();
		return cmprnk-this.getRank();
	}
}



