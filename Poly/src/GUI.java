import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public abstract class GUI     {

	 
	public static void createGUI()
	{
		 JFrame frame = new JFrame ("Polynomial Operations");
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(400, 200);
			
			JPanel panel1 = new JPanel();
			JPanel panel2 = new JPanel();
			JPanel panel3 = new JPanel ();
			JPanel panel4 = new JPanel ();
			JPanel panel5 = new JPanel ();
			JLabel l1 =  new JLabel ("Polynomial 1");
			JLabel l2 =  new JLabel ("Polynomial 2");
			JLabel l3 = new JLabel  ("Result");
			
			
			
			JTextField  tf1 = new JTextField ();
			tf1.setColumns(20);
			 
		    JTextField  tf2 = new JTextField ();	
		    tf2.setColumns(20);
			
		    JTextField  tf3 = new JTextField();
			tf3.setColumns(20);
			tf3.setEditable(false);
		    panel1.add(l1);
			panel1.add(tf1);
			panel1.setLayout(new FlowLayout());
			
			panel2.add(l2);
			panel2.add(tf2);
			panel2.setLayout(new FlowLayout());
			
			JButton b1 = new JButton("Add");
			JButton b2 = new JButton("Subtract");
			JButton b3 = new JButton("Multiply");
			JButton b4 = new JButton ("Divide");
			JButton b5 = new JButton ("Derive P1");
			JButton b6 = new JButton ("Integrate P1");
			panel3.add(b1);
			panel3.add(b2);
			panel3.add(b3);
			panel3.add(b4);
			panel3.setLayout (new FlowLayout());
			
			panel4.add(b5);
			panel4.add(b6);
			panel4.setLayout (new FlowLayout());
			
			
			panel5.add(l3);
			panel5.add(tf3);
			
			
			
			JPanel p = new JPanel();
			p.add(panel1);
			p.add(panel2);
			p.add(panel3);
			p.add(panel4);
			p.add(panel5);
			p.setLayout(new BoxLayout (p, BoxLayout.Y_AXIS));
			
			
			  frame.setContentPane(p);
		      frame.setVisible(true);
		      
		      class  Handler implements ActionListener
		      {  
		           
		    	  public void actionPerformed(ActionEvent e){
			      if(e.getSource()==b1){

			    	  Polynom pol = new Polynom(tf1.getText());
			          Polynom pol2 = new Polynom(tf2.getText());
			    	  Polynom rez=Operations.add(pol,pol2); 
			    	  tf3.setText(rez.printPoli());
			  
			      }
			      
			      if(e.getSource()==b2){
			    	  Polynom pol = new Polynom(tf1.getText());
			          Polynom pol2 = new Polynom(tf2.getText());
			         
			    	  
			    	  Polynom rez=Operations.subtract(pol,pol2);
			    	
			    	  tf3.setText(rez.printPoli());
			  
			      }
			      if(e.getSource()==b3){
			    	  Polynom pol = new Polynom(tf1.getText());  
			          Polynom pol2 = new Polynom(tf2.getText());
			          Polynom rez = Operations.multiply(pol,pol2);
			          tf3.setText(rez.printPoli());
			         
			    	  
			      }
			      if(e.getSource()==b5){
		    	  Polynom pol = new Polynom(tf1.getText());
		    	  Polynom rez= Operations.derive(pol);
		    	  
		    	  tf3.setText(rez.printPoli());
	      
			      }
			      
			      if(e.getSource()==b6){
			    	  Polynom pol= new Polynom(tf1.getText());
			    	  Polynom rez = Operations.integrate(pol);
			    	  tf3.setText(rez.printPold());
			     
			      }
			      
			      if(e.getSource()==b4){
			    	  Polynom pol1=new Polynom(tf1.getText());
			    	  Polynom pol2= new Polynom(tf2.getText());
			    	  Polynom[] rez = Operations.divide(pol1,pol2);
			    	  tf3.setText("Q: "+rez[0].printPold()+" R: "+rez[1].printPold());;
			    	  
			      }
		      }

		  }
		      Handler handler = new Handler();   
		      b1.addActionListener(handler);
		      b2.addActionListener(handler);
	          b3.addActionListener(handler);
	          b4.addActionListener(handler);
	          b5.addActionListener(handler);
	          b6.addActionListener(handler);
		}


	
	
}
