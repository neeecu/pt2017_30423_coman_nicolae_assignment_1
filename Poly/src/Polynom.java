import java.util.*;

 
public class Polynom {

	  String Poly;
	  List<Monom> Polynomial= new ArrayList<Monom>();
	public Polynom(String Poly){
		this.Poly=Poly;
		this.Polynomial=createPol(Poly);
	}
	public Polynom()
	{
		
	}
	 // include the powers of one
	// the variable for the polynom can be any letter a to z
	// we will go throught the string and extract the coefficient and rank for each monom
	
	public List<Monom> createPol(String Str)
	{
		int i=0;
	
		List<Monom> newPol= new ArrayList<Monom>();
		// in oreder to obtain the negative coefficients replace all -  with +-
		// ex -2x^3 +3x^1 ---> +-2x^3 +3x^1
	    Str=Str.replace("-","+-");
		    String[] s = Str.split("\\+");
		    // s contains a list of monoms as strings
		    //  nothing at index 0  for this example :  ,-2x^3, 3x^1
		    for(i=0;i<s.length;i++)
		    {
		    	   //skip iteration if there is no x i an element 
		    	if(s[i].contains("x")==false)
		    		continue;
	    	
	    	    Monom A = new Monom();
	    		String[] temp = s[i].split("x\\^");
	    		// temp [0] = coefficient . temp[1] =rank
	    		// extract coefficient and rank and assign them to the object Monom
	    		// add Monom to the polynomial
	    		if(temp[0].isEmpty())
	    			{A.setCoefficient(1);}
	    		else if(temp[0].equals("-")){ A.setCoefficient(-1);}
	    		else
	    			{A.setCoefficient(Double.parseDouble(temp[0]));}
	    		
	    		if(temp[1].isEmpty())
	    			A.setRank(0);
	    		A.setRank(Integer.parseInt(temp[1]));
	    		newPol.add(A);
	    }
		return  newPol;
	}
	 

   
  
 
 
 public void addMonom ( Monom a)
 {
	 int ok=1;
	for(Monom polymon: this.Polynomial){
		if(a.getRank()==polymon.getRank()){
			polymon.setCoefficient(polymon.getCoefficient()+a.getCoefficient());
			ok=0;
		}
	}
    if(ok==1 && a.getCoefficient()!=0) {
    	this.Polynomial.add(a);
    }
}
 
 public String printPoli( )
 {
	String rez=""; 
	String aux="";
	Collections.sort(this.Polynomial);
	for(Monom i:this.Polynomial)
	{ 
		//if(i.getCoefficient()==0) continue;
		if(i.getCoefficient()>=0){
		 aux="+"+(int)i.getCoefficient()+"x^"+i.getRank();
		}
		else
		aux=(int)i.getCoefficient()+"x^"+i.getRank();
		if(i.getRank()==0 && i.getCoefficient()>0)
			aux="+"+(int)i.getCoefficient();
		else if(i.getRank()==0 && i.getCoefficient()<0){
			aux="-"+(int)i.getCoefficient();
		}
		rez=rez+aux;
	}
	 if(this.Polynomial.isEmpty()==true)
		 return rez="Empty Polynomial";
    else
	return rez;
 }
 
 public String printPold()
 {
	String rez=""; 
	String aux="";
	Collections.sort(this.Polynomial);
	for(Monom i:this.Polynomial)
	{ 
	
		if(i.getCoefficient()>=0){
		 aux="+"+i.getCoefficient()+"x^"+i.getRank();
		}
		else
		aux=i.getCoefficient()+"x^"+i.getRank();
		if(i.getRank()==0 && i.getCoefficient()>0)
			aux="+"+i.getCoefficient();
		else if(i.getRank()==0 && i.getCoefficient()<0){
			aux="-"+i.getCoefficient();
		}
		rez=rez+aux;
	}
	 if(this.Polynomial.isEmpty()==true)
		 return rez="Empty Polynomial";
	 else
	 return rez;

	
 }

public boolean isEmpty()
{
	return this.Polynomial.isEmpty();
}
 
public Monom getMaxMon() {
	
	    return Collections.min(this.Polynomial);
}
}
 

