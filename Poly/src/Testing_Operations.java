import static org.junit.Assert.*;

import org.junit.Test;

public class Testing_Operations  {

	
	Polynom a = new Polynom("x^2+2x^1+1x^0");
	Polynom b = new Polynom ("1x^1+1x^0");
	Polynom summed = new Polynom("x^2+3x^1+2x^0");
	Polynom multiplied = new Polynom ("+1x^3+3x^2+3x^1+1x^0");
    Polynom divided = new Polynom("1x^1+1x^0");
	Polynom subtracted= new Polynom("1x^2+1x^1");
    Polynom derived = new Polynom("+2x^1+2x^0");
    @Test
	
	//tests the functionality of Inverse Function
	public void testInverse() {
		
		
		for(int i=0;i<Operations.inverse(a).Polynomial.size();i++){
		    double  cmp=0-a.Polynomial.get(i).getCoefficient();
			assertEquals(Operations.inverse(a).Polynomial.get(i).getCoefficient(),cmp,0.0);
			
		}
	}

	@Test
	public void testAdd() {
	 
		
		assertEquals(true,Operations.polyEquals(summed,Operations.add(a,b)));
	}

	@Test
	public void testMultiply() {
	
		   assertEquals(true,Operations.polyEquals(multiplied,Operations.multiply(a, b)));
	}

	@Test
	public void testDivide() {
		Polynom[] aux = Operations.divide(a,b);
		assertEquals(true,Operations.polyEquals(divided,aux[0]));
	}

	@Test
	public void testSubtract() {
		assertEquals(true,Operations.polyEquals(subtracted,Operations.subtract(a,b)));
	}

	@Test
	public void testDerive() {
		assertEquals(true,Operations.polyEquals(derived,Operations.derive(a)));
	}


}
